<?php

namespace Log;

abstract class Logger
{    
    public $message;
    public $type;
    protected static $instance;

    public function __call($method, array $p)
    {
        // Проверка существования метода создания лога
        if (!method_exists($this, $method)) {
            throw new ErrorException("Метод {$method} отсутствует");
        }
    }

    protected function __construct() {}

    //Singleton 
    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance=new static();
        }
        return self::$instance;
    }
    
    abstract public function write($message,$type);

    protected function interpolate($message, array $context = array())
    {
      // Построение массива подстановки с фигурными скобками
      // вокруг значений ключей массива context.
      $replace = array();
      foreach ($context as $key => $val) {
          $replace['{'.$key.'}'] = $val;
      }
 
      // Подстановка значений в сообщение и возврат результата.
      return strtr($message, $replace);
    }

    /**
     * Далее идут методы для реализации уровней протоколирования
     *
     *
     * Авария, система неработоспособна.
     *
     * @param строка $message
     * @param массив $context
     * @return null
     */

    public function emergency($message, array $context = array())
    {
        $this->write($this->interpolate($message, $context), __FUNCTION__);
    }
 
    /**
     * Тревога, меры должны быть предприняты незамедлительно.
     *
     * @param строка $message
     * @param массив $context
     * @return null
     */
    public function alert($message, array $context = array()){
        $this->write($this->interpolate($message, $context), __FUNCTION__);      
        }
 
    /**
     * Критическая ошибка, критическая ситуация.
     *
     * @param строка $message
     * @param массив $context
     * @return null
     */
    public function critical($message, array $context = array())
    {
        $this->write($this->interpolate($message, $context), __FUNCTION__);       
    }
 
    /**
     * Ошибка на стадии выполнения, не требующая неотложного вмешательства,
     * но требующая протоколирования и дальнейшего изучения.
     *
     * @param строка $message
     * @param массив $context
     * @return null
     */
    public function error($message, array $context = array())
    {
        $this->write($this->interpolate($message, $context), __FUNCTION__);      
    }
 
    /**
     * Предупреждение, нештатная ситуация, не являющаяся ошибкой.
     *
     * @param строка $message
     * @param массив $context
     * @return null
     */
    public function warning($message, array $context = array())
    {
        $this->write($this->interpolate($message, $context), __FUNCTION__);       
    }
 
    /**
     * Замечание, важное событие.
     *
     * @param строка $message
     * @param массив $context
     * @return null
     */
    public function notice($message, array $context = array())
    {
        $this->write($this->interpolate($message, $context), __FUNCTION__);    
    }
 
    /**
     * Информация, полезные для понимания происходящего события.
     *
     * @param строка $message
     * @param массив $context
     * @return null
     */
    public function info($message, array $context = array())
    {
        $this->write($this->interpolate($message, $context), __FUNCTION__);
    }
 
    /**
     * Детальная отладочная информация.
     *
     * @param строка $message
     * @param массив $context
     * @return null
     */
    public function debug($message, array $context = array())
    {
        $this->write($this->interpolate($message, $context), __FUNCTION__);
    }
}
