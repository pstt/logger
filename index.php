<?php

namespace Log;

require __DIR__ . '/vendor/autoload.php';

//конфигурация для логгера
$config = [
    'logger' => '\Log\File', // логгер 
    'file' => $_SERVER['DOCUMENT_ROOT'].'/Logger/info_error.txt', // путь для лог-файла
    // конфигурация для подключения к Базе данных
    //'host'=> "localhost",
    //'db' => "logs",
    //'user' => "admin",
    //'pass' => "admin"
];

$context = array('username' => 'USER');
$log = LoggerFactory::create($config);
try {
    $log->emergency("User {username} created",$context);
    $log->info("User logout");
    echo "лог записан ".$log->logger;
} catch (ErrorException $ee) {
    echo $ee->getMessage();
}
