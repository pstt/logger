<?php

namespace Log;

use Log\Logger;

class Db extends Logger
{
    public function write($message="", $type="") // функция записи лога
    {
        // если сообщение - массив, то преобразуем в строку
        if (is_array($message)) {
            $message = implode(", ", $message);
        }
        $date=strval(date("Y-m-d H:i:s")); // инициализируем дату
        try {
            // записываем в БД
            $dbh = new \PDO("mysql:host=$this->host;dbname=$this->db;charset=utf8", $this->user, $this->password);
            $STH = $dbh->prepare("INSERT INTO logs SET `date`='$date',`type`='$type', `message`='$message'");
            $STH->execute();
            return true;
        } catch(\Exception $ex) {
            echo $ex->getMessage();
        }
    }
}
