<?php

namespace Log;

use Log\Logger;

class File extends Logger
{
    public function write($message="", $type="") // функция записи а лог-файл
    {
        // если сообщение - массив, то преобразуем в строку
        if (is_array($message)) {
            $message = implode(", ", $message);
        }
        $file_path=$this->file; //определяем путь хранения лог-файла
        $text = date("Y-m-d H:i:s")."[".$type."]-".htmlspecialchars($message)."\r\n";
        $handle = fopen($file_path, "a+"); //открываем лог-файл
        fwrite ($handle, $text); //записываем в лог-файл данные
        fclose($handle); //закрываем лог-файл
        return true;
    }
}
