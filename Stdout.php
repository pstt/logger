<?php

namespace Log;

use Log\Logger;

class Stdout extends Logger
{
    public function write($message="", $type="etc") // функция записи а лог-файл
    {
        // если сообщение - массив, то преобразуем в строку
        if (is_array($mess)) {
            $message = implode(", ", $message);
        }
        $text = date("Y-m-d H:i:s")." [". $type ."] - " . htmlspecialchars($message)."\r\n";
        fwrite(STDOUT,$text); //выводим в STDOUT
    }
}
