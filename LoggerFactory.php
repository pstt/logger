<?php

namespace Log;

use Log\Logger;
use Log\ErrException;

class LoggerFactory 
{
    // функция для создания объкта логгера
    public static function create($config)
    {
        try {
            // проверка существования класса логгера, заданного в конфигурационном файле
            if (class_exists($config['logger'])) {
                $logger=$config['logger']::getInstance(); // создание объекта логгера
                foreach ($config as $name => $value) { // инициализация параметров вывода
                $logger->$name = $value;
                }
                return $logger;
            } else {
                throw new ErrorException("class {$config['logger']} not found");
            }
        } catch (ErrorException $e) {
            echo $e->getMessage();
        } catch (\Exception $ex) {
            echo $ev->getMessage();
        }
    }
}
